FROM node:16.0.0

RUN apt update && apt install vim -y

EXPOSE 3000/tcp

WORKDIR /app

RUN pwd

COPY . .

CMD npm start

